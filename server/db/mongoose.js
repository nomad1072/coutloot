var mongoose = require('mongoose');
var config = require('../config/config.js');

mongoose.Promise = global.Promise;
mongoose.connect(config.mongo_url)

module.exports = {
    mongoose
};