var express = require('express');
var router = express.Router();
var loginController = require('../controllers/login')
var registerController = require('../controllers/register')
var User = require('../models/User')

router.post('/login', loginController.login);
router.post('/register', registerController.addUser);

router.all('*', function(req, res, next) {
    var token = req.header('jwt-auth');
    
    User.findByToken(token)
        .then((user) => {
            if (!user) {
                return Promise.reject();
            }
            req.user = user;
            req.token = token;
            next();
        }).catch((e) => {
            res.status(401).send();
        })
});

module.exports = {
    router
};