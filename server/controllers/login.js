var {User} = require('../models/User')

exports.login = (req, res) => {
    var email = req.body.email;
    var password = req.body.password;
    User.findByCredentials(email, password)
        .then((user) => {
            user.generateAuthToken()
                .then((token) => {
                    res.header('jwt-auth', token).send(user);
                })
        })
        .catch((e) => {
            res,status(400).send();
        })
};