var {User} = require('../models/User')
var nodemailer = require('nodemailer');

// var transporter = nodemailer.createTransport({
//     service: 'gmail',
//     auth: {
//         user: 'siddharthlanka@gmail.com',
//         pass: ''
//     }
// });

// const mailOptions = {
//     from: 'siddharthlanka@gmail.com',
//     to: 'somerandomuser@somedomain',
//     subject: 'New Registration',
//     html: '<p>Yo! Thanks for registering</p>'
// }

exports.addUser = (req, res) => {
    var user = new User({});

    user.email = req.body.email;
    user.password = req.body.password;
    user.name = req.body.name;
    user.country = req.body.country;

    user.save()
        .then(() => {
            return user.generateAuthToken();
        })
        .then((token) => {
            // const options = {...mailOptions, to: user.email}
            // transporter.sendMail(mailOptions, function (err, info) {
            //     if (err) {
            //         console.log(err)
            //     } else {
            //         console.log(info);
            //     }
            // });
            res.header('jwt-auth', token).send(user)
        })
        .catch((e) => {
            res.status(400).send(e);
        })
}