var express = require('express');
var path = require('path')
var bodyParser = require('body-parser')
var app = express();
var {router} = require('./routers/index')

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({limit: "50mb"}));
app.use('/api/v1', router);

var repoRoot = path.join(__dirname,'/..');

app.use(express.static(repoRoot+"/client/build/"));

app.use("*", (req, res) => {
    res.sendFile(`/client/build/index.html`, {root: repoRoot})
});

app.listen(8080, function() {
    console.log('Server started at 8080');
})