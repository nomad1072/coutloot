var {mongoose} = require('../db/mongoose');
var jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

var UserSchema = new mongoose.Schema({
    email: {type: String, unique: true, required: true},
    password: {type: String, required: true},
    name: {type: String, required: true},
    country:{type: String, required: true},
    tokens: [{
        access: String,
        token: String
    }]
})


UserSchema.statics.findByToken = function (token) {
    var User = this;
    var decoded;
  
    try {
      decoded = jwt.verify(token, 'abc1234');
    } catch (e) {
      return new Promise((resolve, reject) => {
        reject('No Token found! This is an invalid token');
      });
      // return Promise.reject();
    }
}

UserSchema.methods.generateAuthToken = function () {
    var user = this;
    var access = 'auth';
    var token = jwt.sign({_id: user._id.toHexString(), access}, 'abc1234').toString();

    user.tokens.push({access, token});

    return user.save()
            .then(() => {
                return token;
            })
}

UserSchema.statics.findByCredentials = function(email, password) {
    var User = this;
    return User.findOne({ email })
            .then((user) => {
                if (!user) {
                    return Promise.reject('User does not exist! You might want to register him first!')
                }

                return new Promise((resolve, reject) => {
                    bcrypt.compare(password, user.password, (err, res) => {
                        if (res) {
                            resolve(user);
                        } else {
                            reject('Passwords do not match! Please try again')
                        }
                    })
                })
            })
            .catch((err) => {
                console.log(err);
            })
}

UserSchema.pre('save', function(next) {
    var user = this;
    if (user.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(user.password, salt, (err, hash) => {
                user.password = hash;
                next();
            })
        })
    } else {
        next();
    }
})

var User = mongoose.model('User', UserSchema);
module.exports = {
    User
}