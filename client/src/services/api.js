import axios from "axios";

const api_path = "/api/v1";
const login_path = `${api_path}/login`;
const register_path =  `${api_path}/register`;
export function login(email, password) {
    let user = {
        email: email,
        password: password
    };
    return new Promise((resolve, reject) => {
        axios.post(login_path, user)
        .then(res => {
            resolve(res);
        })
        .catch((e) => {
            reject(e);
        })
    })
}

export function register(email, password, name, country) {
    let user = {
        email,
        password,
        name,
        country
    };
    return new Promise((resolve, reject) => {
        axios.post(register_path, user)
        .then(res => {
            resolve(res);
        })
        .catch((e) => {
            reject(e);
        })
    })
}