import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Home from './Home'
import Login from './views/login'
import Register from './views/register'

ReactDOM.render(
    <Router>
        <div>
            <Route exact path="/" component={Home}/>
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register}/>
        </div>
    </Router>, 
    document.getElementById('root')
);