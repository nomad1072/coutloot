import React from 'react';
import {Link} from 'react-router-dom'

class Home extends React.Component {
    render() {
        return (
            <React.Fragment>
                <p>This is the home component</p>
                    <ul>
                        <li>           
                            <Link to="/login">Login</Link>
                        </li>  
                        <li>
                            <Link to="/register">Register</Link>
                        </li>
                    </ul>
            </React.Fragment>
        );
    }
}

export default Home;