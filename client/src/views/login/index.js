import React from 'react'
import {login} from '../../services/api'

class Login extends React.Component {
    state = {
        auth: {},
        text: ''
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        const {email, password} = this.state.auth;
        let res = await login(email, password);
        if (res.status === 200) {
            this.setState({text: 'Logged In!'});
        } else {
            this.setState({text: 'Unable to login, check credentials'})
        }
        console.log('res: ', res);
    }

    handleInputChange = (e) => {
        let key = e.target.name;
        let value = e.target.value;
        this.setState(prevState => ({
            auth: {
                ...prevState.auth,
                [key]: value
            }
        }))
        console.log(JSON.stringify(this.state, null, 3));    
    }
    render() {
        return (
            <React.Fragment>
                <p>{this.state.text}</p>
                <p>This is the login component</p>
                <div>
                    You can login from here
                </div>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <span>Email: </span>
                        <input type="email"  name="email" className="email" onChange={this.handleInputChange} />
                    </div>
                    <div>
                        <span>Password: </span>
                        <input type="password" name="password" className="password" onChange={this.handleInputChange} />
                    </div>
                    <button type="Submit">Submit</button>
                </form>
            </React.Fragment>
        );
    }
}

export default Login;