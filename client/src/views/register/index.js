import React from 'react';
import {register} from '../../services/api'


class Register extends React.Component {
    
    state = {
        auth: {},
        text: ''
    }
    
    handleSubmit = async(e) => {
        e.preventDefault()
        const {email, password, name, country} = this.state.auth;
        let res = await register(email, password, name, country)
        console.log('User Registered');
        console.log(res);
        if (res.status === 200) {
            this.setState({text: 'User Registered!'})
        } else {
            this.setState({text: 'User Registration failed :('})
        }
    }

    handleInputChange = (e) => {
        let key = e.target.name;
        let value = e.target.value;
        this.setState(prevState => ({
            auth: {
                ...prevState.auth,
                [key]: value
            }
        }))
    }

    render() {
        return (
            <React.Fragment>
                <p>{this.state.text}</p>
                <p>This is the Register component</p>
                <div>
                    You can register from here
                </div>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <span>Email: </span>
                        <input type="email"  name="email" className="email" onChange={this.handleInputChange} />
                    </div>
                    <div>
                        <span>Password: </span>
                        <input type="password" name="password" className="password" onChange={this.handleInputChange} />
                    </div>
                    <div>
                        <span>Name: </span>
                        <input name="name" className="password" onChange={this.handleInputChange} />
                    </div>
                    <div>
                        <span>Country: </span>
                        <input name="country" className="country" onChange={this.handleInputChange} />
                    </div>
                    <button type="Submit">Submit</button>
                </form>
            </React.Fragment>
        );
    }
}

export default Register;